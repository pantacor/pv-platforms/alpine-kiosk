#!/bin/sh

# start X server

if [ -z "$ALPINE_KIOSK_NORUN" ]; then
	exec startx
fi

