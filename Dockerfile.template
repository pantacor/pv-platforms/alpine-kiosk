FROM alpine as qemu

RUN if [ -n "${QEMU_ARCH}" ]; then \
		wget -O /qemu-${QEMU_ARCH}-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-${QEMU_ARCH}-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-${QEMU_ARCH}-static; \
	fi; \
	chmod a+x /qemu-${QEMU_ARCH}-static

FROM ${ARCH}/alpine:3.11

COPY --from=qemu /qemu-${QEMU_ARCH}-static /usr/bin/

RUN adduser -D -h /home/kiosk kiosk; sed -i 's/^\(tty.*\)$/\1tty/' /etc/group
RUN apk update; \
	apk add --update \
		mesa-egl \
		xf86-video-fbdev \
		xf86-video-vesa \
		xf86-input-mouse \
		xf86-input-keyboard \
		dbus \
		setxkbmap \
		kbd \
		xrandr \
		xorg-server \
		xinit \
		xset; \
	rm -rf /var/cache/apk/*

RUN apk update; \
	apk add --update busybox-extras \
		chromium \
		wget \
		jq; \
	rm -rf /var/cache/apk/*

ADD files /

RUN chown -R kiosk:kiosk /home/kiosk

WORKDIR /workspace

CMD [ "/bin/login", "-f", "kiosk" ]
